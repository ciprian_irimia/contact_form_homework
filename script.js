"use strict";

const form = document.querySelector("#form");
const firstName = document.querySelector("#firstName");
const lastName = document.querySelector("#lastName");

form.addEventListener("submit", (e) => {
  e.preventDefault();

  testInputs();
  validateForm();
});

function testInputs() {
  const firstNameValue = firstName.value.trim();
  const lastNameValue = lastName.value.trim();

  if (firstNameValue === "") {
    loadErrorFor(firstName);
  } else {
    loadSuccessFor(firstName);
  }

  if (lastNameValue === "") {
    loadErrorFor(lastName);
  } else {
    loadSuccessFor(lastName);
  }

  if (firstNameValue !== "" && lastNameValue !== "") {
    console.log("test ok");
  } else {
    console.log("test fail");
  }
  function loadErrorFor(input) {
    const formCheck = input.parentElement;
    formCheck.classList.remove("success");
    formCheck.classList.add("error");
  }

  function loadSuccessFor(input) {
    const formCheck = input.parentElement;
    formCheck.classList.remove("error");
    formCheck.classList.add("success");
  }
}

function validateForm() {
  const formControls = document.querySelectorAll("#form .name-details");
  console.log(formControls);

  // formControls.forEach((control) => {
  //   if (control.classList.contains("success")) {
  //     console.log("success");
  //   } else {
  //     console.log("error");
  //   }
  // });

  /*for (const control of formControls) {
    // if (control.classList.contains("success")) {
    //   isValid != true;
    // } else {
    //   isValid = false;
    // }

    if (!control.classList.contains("success")) {
      // console.log(false);
      return;
    }
  }
  // console.log(true);*/

  const isValid = Array.from(formControls).every((control) =>
    control.classList.contains("success")
  );

  return isValid && showAlertBanner();
  // console.log(isValid);
}

function showAlertBanner() {
  const validAlertDisplay = document.querySelector("#alertDisplay");
  const spanBanner = document.querySelector("#insertName");
  spanBanner.textContent = firstName.value;
  validAlertDisplay.classList.add("alert-display-visible");

  const closeBanner = document.querySelector("#close-alert");

  const closeHandler = () => {
    validAlertDisplay.classList.remove("alert-display-visible");
    closeBanner.removeEventListener("click", closeHandler);
  };

  closeBanner.addEventListener("click", closeHandler);
}
